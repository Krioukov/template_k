<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if ($icon) { ?>
    <link href="<?php echo $icon; ?>" rel="icon" />
    <?php } ?>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <?php
    $_dr	= $_SERVER['DOCUMENT_ROOT'];
	$full	= str_replace('\\','/',__DIR__).'/../../';
	$tpl	= str_replace($_dr, '', $full)
	?>
    <link href="<?=$tpl;?>css/bootstrap.css?123123123" rel="stylesheet">
    <link href="<?=$tpl;?>css/template.css" rel="stylesheet">
    <link href="<?=$tpl;?>css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="<?=$tpl;?>js/source/jquery.fancybox.css" rel="stylesheet">
    <script src="<?=$tpl;?>js/jquery-3.1.1.min.js"></script>
    <script src="<?=$tpl;?>js/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="<?=$tpl;?>js/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="<?=$tpl;?>js/bootstrap.min.js"></script>
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <script src="<?=$tpl;?>js/common.js" type="text/javascript"></script>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php echo $google_analytics; ?>
</head>
<body class="<?php echo $class; ?>"><?//=$tpl;?>

    <!-- SUPERHEAD -->
    <div class="top">
        <div class="container top-inner">
            <ul>
                <li class="col-md-2"><a id="feedback" class="fancybox.ajax" href="http://korzh.mkbg.ru/index.php?route=information/contact"><i class="ic pe-7s-mail"></i><span class="caption">Обратная связь</span></a></li>
                <li class="col-md-2"><a id="return_call" class="fancybox.ajax" href="http://korzh.mkbg.ru/index.php?route=module/backcall"><i class="ic pe-7s-call"></i><span class="caption">Заказать звонок</span></a></li>
                <li id="credentials" class="right"><a href="#" onclick="return false;"><i class="ic pe-7s-user"></i><span class="caption"><?php echo $text_account; ?></span></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <?php if ($logged) { ?>
                        <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                        <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                        <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                        <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
                        <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                        <?php } else { ?>
                        <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                        <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>     
        </div>    
    </div>
    <?$IMAGES   = str_replace($_SERVER['DOCUMENT_ROOT'],'',str_replace('\\','/',__DIR__)).'/../../img/';?>
    <!-- HEADER (logo, tlf, etc) -->
    <div class="header">
        <div class="container">
            <div class="left-c">
                <div class="logo col-md-6"><a href="<?php echo $home; ?>"><img style="margin: 10px 0 0 -3px;" src="<?=$IMAGES;?>logo.svg"></a></div>
                <div class="contacts"><span class="code colored"><?=$tel_code;?></span><span class="numbers"><?php echo $tel_body; ?></span>
                    <div class="address"><?php echo $address; ?></div>
                </div>
            </div>
            <div class="right-c">
                <div class="shop-features">
                    <ul>
                        <li>
                            <div class="sf-wrap icon">
                                <a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>">
                                    <span class="icons"></span>
                                </a>    
                            </div>
                            <div class="sf-wrap-t">
                                <div class="colored" style="text-align: center;">
                                    <a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a>
                                </div>
                                <div id="cart"><?php echo $cart; ?></div>
                            </div>
                        </li>
                        <li>
                            <div class="sf-wrap icon">
                                <a href="<?php echo $wishlist; ?>">
                                    <span class="icons"></span>
                                </a> 
                            </div>
                            <div class="sf-wrap-t">
                                <div class="colored">
                                    <a href="<?php echo $wishlist; ?>" title="<?php echo $text_wishlist; ?>">Закладки</a>
                                </div>
                                <div id="wishlist-total">Товаров (<span><?php echo $text_wishlist; ?></span>)</div>
                            </div>
                        </li>    
                    </ul>    
                </div>
                <div class="search">
                    <?php echo $search; ?>
                </div>
            </div>    
        </div>
    </div>
<?Print_r($this->config);?>
    <!-- ARTICLES -->
    <div class="art-menu row">
        <div class="container">
            <ul class="menu">
                <li class="frst-level" id="home"><a href="<?php echo $home; ?>"><span class="pe-7s-home"></span></a></li>
                <?$i    = 0;?>
                <?foreach ( $informations as $article ):?>
                <?++$i;?>
                <li class="frst-level" menu-index="<?=$i;?>"><a href="<?=$article['href'];?>"><?=$article['title'];?></a></li>
                <?endforeach;?>
                <li class="other"><a href="#">...</a>
                     <div class="catalog-2nd-level">
                        <div class="js-menu-othercol">
                            <?$i    = 0;?>
                            <?foreach ( $informations as $article ):?>
                            <?++$i;?>
                            <a class="menu2" href="<?=$article['href'];?>" menu-index="<?=$i;?>"><?=$article['title'];?></a>
                            <?endforeach;?>
                        </div>
                    </div>
                </li>    
            </ul>
        </div>
    </div>
   
    <!-- MENU -->
    <div class="cat-menu row">
        <div class="container">
            <div class="wrap">
                <ul class="menu">
                <?$i    = 0;?>
                <?$k    = 0;?>
                <?foreach ( $categories as $first_lev_item ):?>
                    <?++$i;?>
                    <?$children   = $first_lev_item['children'];?>
                    <li class="frst-level <?= count($children) > 0?'hasInner':'';?>" menu-index="<?=$i;?>"><a href="<?=$first_lev_item['href'];?>"><?=$first_lev_item['name'];?></a>
                        <?if ( count($children) > 0 ):?>
                        <div class="catalog-2nd-level">
                            <?$q_incolumn = ceil(count($children)/3);?>
                            <?$k = 0;//3-columns?>
                            <?$columned = array();?>
                            <?foreach ( $children as $chld ):?>
                                <?$columned[$k][] = $chld;?>
                                <?++$k;?>
                                <?if ($k == 3) $k = 0;?>
                            <?endforeach;?>    
                            <?foreach ( $columned as $c_children):?>
                                <div class="js-menu-col">
                                    <?$q = 0;?>
                                    <?foreach ( $c_children as $chld ):?>
                                    <a class="menu2" href="<?=$chld['href'];?>"><?=$chld['name'];?></a>
                                        <?$q++;?>
                                        <?if ( $q >= $q_incolumn ) break;?>
                                    <?endforeach;?>
                                </div>
                            <?endforeach;?>
                        </div>
                        <?endif;?>
                    </li>
                <?endforeach;?>    
                    
                    <li class="other"><a href="#">...</a>
                        <div class="catalog-2nd-level">
                            <div class="js-menu-othercol">
                                <?$i    = 0;?>
                                <?foreach ( $categories as $first_lev_item ):?>
                                    <?++$i;?>
                                <a class="menu2" menu-index="<?=$i;?>" href="<?=$first_lev_item['href'];?>"><?=$first_lev_item['name'];?></a>
                                <?endforeach;?> 
                            </div>
                        </div>
                    </li>
                </ul>
            </div>    
        </div>
    </div>
    
    <!-- MENU FOR PHONE MODE -->
    <div class="phone-menu row">
        <div class="container">
            <div class="wrap">
                <ul class="menu">
                    <li class="main_parent btn ph_mnu col-sm-4 col-xs-2">
                        <a href="#" onclick="return false;"><span class="pe-7s-menu"></span></a>
                        <ul class="second_level">
                            <li class="group">
                                Пользователь
                            </li>
                            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a> / <a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                            <li class="group">
                                Товары
                            </li>
                            <?foreach ( $categories as $first_lev_item ):?>
                            <li>
                                <a href="<?=$first_lev_item['href'];?>"><?=$first_lev_item['name'];?></a>
                            </li>
                            <?endforeach;?>
                            <li class="group">
                                Информация
                            </li>
                            <?foreach ( $informations as $article ):?>
                            <li>
                                <a href="<?=$article['href'];?>"><?=$article['title'];?></a>
                            </li>
                            <?endforeach;?>
                        </ul>
                    </li>
                    <li class="main_parent l_logo col-sm-6 col-xs-6">
                        <a href="<?php echo $home; ?>"><img src="<?=$IMAGES;?>logo.svg"></a>
                    </li>
                    <li class="search_parent btn col-sm-4 col-xs-2 search_btn">
                        <a href="#" onclick="return false;"><span class="pe-7s-search"></span></a>
                        <div id="search_wrap">
                            <?php echo $search; ?> <div class="s_close btn"><span class="pe-7s-close"></span></div>
                        </div>
                    </li>
                    <li class="main_parent btn col-sm-4 col-xs-2">
                        <a href="<?php echo $shopping_cart; ?>"><span class="pe-7s-cart"></span></a>
                    </li>
                </ul>
            </div>
        </div>    
    </div>
    
    <!-- CONTENT -->
    <div class="content">
