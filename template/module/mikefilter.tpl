<?if (count($ierarchy) > 0):?>
    
    <?if($ajax !== false){
        echo trim(json_encode($ajax_answer));
    }?>

    <?if($ajax === false):?> 
    <div class="panel panel-default">
        <div class="filter-wrap">
            <h3>Goods parameters</h3>
            <a class="collapsable">Price:</a>
            <div class="price_fields_wrap ">
                <div id="slider"></div>
            </div>
            <div id="ajax">  
                <ul class="filter">
                    <?foreach ($ierarchy as $id=>$attribute):?>
                    <li ><a class="collapsable"><?=$attribute['NAME']?></a>
                        <ul class="collapsed collapse in">
                            <?foreach($attribute['VALUES'] as $key=>$value):?>
                                <?//$id   	= $attribute['ID'].'_'.base64_encode($value['VALUE']);?>
                                
                                <li id="li_<?=$key?>"><input id="<?=$key?>" type="checkbox" <?=$saved[$key]['value']=='true'?'checked="checked"':'';?>></input><label for="<?=$key?>"><?=$value['VALUE'];?> (<span class="option_counter" id="<?=$key?>_counter"><?=$value['QUANTITY'];?></span>)</label></li>
                            <?endforeach;?>    
                        </ul>
                    </li>    
                    <?endforeach;?>
                </ul> 
            </div>
        </div> 
        <div class="button_wrap">
            <button id="filter_id" type="button" class="btn btn-primary btn-block" onclick="MKFL.submitFilter()"> Подобрать</button>
        </div>    
    </div>

        <script>
			MKFL = {
				category			: <?=$category_id;?>,
				timeOutId   		: false,
                filterResponse      : {},
                filterData          : {},
				submitFilter		: function(){
                    var values = $("#slider" ).slider( "option", "values" );
                    console.log(values);
                    MKFL.filterData['price']    = values;
					$('input', '.filter-wrap').each(function(){
						switch(this.type){
							case 'checkbox':
								MKFL.filterData[this.id]	= this.checked;
							break;
							case 'text':
							case 'radio':
								MKFL.filterData[this.id]	= this.value;
							break;
							default:
								MKFL.filterData[this.id]	= '';
						}		
					});
					MKFL.filterData['path']	= MKFL.category;
					MKFL.filterData['ajax']	= 'true';
					$.post('index.php?route=module/mikefilter', MKFL.filterData, MKFL.insertData);
					return false;
				},
				insertData			: function(data){
                    $('span.option_counter').each(function(){
                        this.innerHTML   = '0';
                    });
                    console.log(data);
                    if (data.trim() !== ''){
                        MKFL.filterResponse = JSON.parse(data); 
                        //checks
                        
                        for (key in MKFL.filterResponse.ierarchy){
                            for (id in MKFL.filterResponse.ierarchy[key]['VALUES']){
                                var quantity = MKFL.filterResponse.ierarchy[key]['VALUES'][id]['QUANTITY'];
                                $('span#' + id + '_counter').get(0).innerHTML   = quantity;
                            }
                        }
                        //slider
                        if(typeof(MKFL.filterResponse.priceRange)!=='undefined'){
                            jQuery("#slider").slider({
                                min: Number(MKFL.filterResponse.priceRange.min),
                                max: Number(MKFL.filterResponse.priceRange.max)
                            });
                        }
                    }    
				},
				bindSubmitAction 	: function (){
					$('input', 'ul.filter').click(function(){
						if(MKFL.timeOutId !== false){
							clearTimeout(MKFL.timeOutId);
						}
						MKFL.timeOutId   = setTimeout(function(){
							MKFL.submitFilter();
						}, 50); //filter refreshing    
					});
				},
				bindColapseAction	: function (){
					$('a.collapsable', 'ul.filter').click(function(){      
						$('.collapsed', $(this).parents('li').get(0)).collapse('toggle');
					});
				},
                makeSliders         : function (){
                    jQuery("#slider").slider({
                        min: <?=$price_range['price']['minprice']?>,
                        max: <?=$price_range['price']['maxprice']?>,
                        values: [<?=$price_range['price']['minprice']?>,<?=$price_range['price']['maxprice']?>],
                        range: true,
                        change: MKFL.sliderOnChange 
                    });
                },
                sliderOnChange      : function(sl, ui){
                    MKFL.submitFilter();
                }
			}	
            MKFL.bindSubmitAction();
            MKFL.bindColapseAction();
            MKFL.makeSliders();
            

        </script>

        <style>
            .filter-wrap{
                padding: 10px;
            }

            ul.filter {
                list-style: none;
                padding: 0;
            }
            
            ul.filter li {
                margin: 0;
            } 
            
            ul.filter li a, a.collapsable {
                cursor: pointer;
                display: block;
                background: #eee;
                padding: 5px;
                margin: 0 0 6px 0;
            }
            
            ul.filter li ul {
                list-style: none;
                padding: 0 0 0 15px;
            }
            
            ul.filter li ul li input {
                display: block;
                margin: 3px 0 0 0;
                float: left;
            }
            
            ul.filter li ul li label{
                padding: 0 0 0 10px;
            }
            
            div.min_price{
                float: left;
            }
            
            div.max_price{
                float: left;
            }
            
            div.price_slider_wrap{
                clear: both;
            }
        </style>
    <?endif;?>
<?endif;?> 