$('#credentials a').click(function(){
    $('#credentials > .dropdown-menu').show();
});
// - - - - - MENUs - - -
class allMenu {
    constructor(el) {        
        this.el     = $(el);
    }
    
    allElementsWidth(){
        var sum         = 0;
        var quantity    = 0;
        this.el.find("li.frst-level").each(function(){
            sum     +=  $(this).width()
                        + parseInt($(this).css('margin-left')) 
                        + parseInt($(this).css('margin-right'))
                        + parseInt($(this).css('padding-left'))
                        + parseInt($(this).css('padding-right'));
            ++quantity;
        });
        this.q  = quantity;
        return sum;
    }
    
    init(){
        this.prntWdh    = this.el.width() 
                            //+ parseInt(this.el.css('margin-left')) 
                            //+ parseInt(this.el.css('margin-right'))
                            - parseInt(this.el.css('padding-left'))
                            - parseInt(this.el.css('padding-right'));
        this.allElWdth  = this.allElementsWidth(); //console.log(this.prntWdh + "<" + this.allElWdth);       
        if ( this.prntWdh < this.allElWdth ){
            this.el.find(".other").show();
            this.restruct();
        }else{
            this.el.find(".other").hide();
            this.restruct();
        }
    }
    
    restruct(){
        var total       = this.prntWdh; 
        var sum         = 0;
        var last_width  = 50;
        var el          = this.el;
        var did_it      = false;
        
        // - - close active (b) - -
        $('.active', el).removeClass('active');
        // - - close active (e) - -
        
        // - - last element (b) - -
        this.el.find("li.frst-level").each(function(){
            var el_mrgn   = parseInt($(this).css('margin-right')) + parseInt($(this).css('margin-left'));
        
            var item_width  = $(this).outerWidth(true);
            //console.log(sum + item_width + 50 + " > " + total);            
            if ( sum + item_width + 50 > total ){
                if (!$(this).hasClass("other")){
                    $(this).hide();/*----*/
                    $('.js-menu-othercol [menu-index=' + $(this).attr('menu-index') + ']', el).css({display:'block'});
                    last_width  = item_width;
                }
                if ( did_it == false) {
                    el.find("li.other").width(total - sum - el_mrgn - 2); //2 - borders of item
                    did_it  = true;
                }    
            }else{
                if (!$(this).hasClass("other")){
                    $(this).show();/*----*/
                    $('.js-menu-othercol [menu-index=' + $(this).attr('menu-index') + ']', el).hide();
                }  
            }
            sum     +=  item_width;
        });
        this.last_width = last_width;
        // - - last element (e) - -
        
         // - - 2nd level (b) - -         
        switch ( true ){
            case this.el.width() >= 998 :
                this.el.find(".frst-level .catalog-2nd-level").width(this.el.width() * 0.6);
            break;
            case this.el.width() <= 670 :
                this.el.find(".frst-level .catalog-2nd-level").width(this.el.width() * 1);
            break;
            case this.el.width() <= 998 :
                this.el.find(".frst-level .catalog-2nd-level").width(this.el.width() * 0.8);
            break;
        }
        
        // - - 2nd level (e) - -
        
        // - - 2nd lev. right edge (b) - -
        var right_edge  = this.el.position().left + this.el.width();
        var left_edge   = this.el.position().left;

        this.el.find(".frst-level").each(function(){
            $(this).hover(function(){
                if ( $(".catalog-2nd-level", this).position() != undefined ){
                    // console.log(" - - - - - - -");
                    // console.log(left_edge);
                    // console.log($(this).position().left);
                    // console.log($(".catalog-2nd-level", this).width());
                    // console.log(right_edge);
                    // console.log(" - - - - - - -");
                    if ( (left_edge + $(this).position().left + $(".catalog-2nd-level", this).width()) > right_edge ){
                        var dif = (left_edge + 
                                    $(this).position().left + 
                                    $(".catalog-2nd-level", this).width()) - right_edge;
                        // console.log(dif);
                        $(".catalog-2nd-level", this).css({left: -dif - 3 + "px"});
                    }else{
                        $(".catalog-2nd-level", this).css({left: -1 + "px"});
                    }
                }
            });    
        });
        
         //width for OTHER (b)
        var other = this.el.find(".other");
        if ( other.width() < 180 ){
            $(".catalog-2nd-level", other).width(200);    
        }else{
            $(".catalog-2nd-level", other).width(other.width() * 1.5);
        }
        //width for OTHER (e)
        
        this.el.find(".other").each(function(){
            $(this).hover(function(){
                if ( $(".catalog-2nd-level", this).position() != undefined ){
                    // console.log(" - - - - - - -");
                    // console.log(left_edge);
                    // console.log($(this).position().left);
                    // console.log($(".catalog-2nd-level", this).width());
                    // console.log(right_edge);
                    // console.log(" - - - - - - -");
                    if ( (left_edge + $(this).position().left + $(".catalog-2nd-level", this).width()) > right_edge ){
                        var dif = (left_edge + 
                                    $(this).position().left + 
                                    $(".catalog-2nd-level", this).width()) - right_edge;
                        // console.log(dif);
                        $(".catalog-2nd-level", this).css({left: -dif - 3 + "px"});
                    }else{
                        $(".catalog-2nd-level", this).css({left: 1 + "px"});
                    }
                }
            });    
        });
        
        // this.el.find(".catalog-2nd-level").each(function(){
            // console.log($(this).parent('li'));
            // console.log($(this).parent('li').position().left); 
        // });
        // - - 2nd lev. right edge (e) - -
    }
}

// - - PHONE MENU (b) - -
class phoneMenu {
    constructor(el) {        
        this.el     = $(el);
    }
    
    init(){
        var cont    = this.el.find('.container');
        //menu
        this.el.find("ul.second_level").width(cont.width() - 2); 
        
        //search
        this.el.find("div#search_wrap").width(cont.width() - 2);
        this.el.find("div#search_wrap #search").width(cont.width() - 2 - 60);
        
        var b_left  = this.el.find('ul.menu').position().left;
        var n_left  = -(this.el.find('li.search_btn').position().left - b_left + 2);
        this.el.find('div#search_wrap').css({'left':n_left+'px'});
    }
}
// - - PHONE MENU (e) - -

$(".art-menu ul li").hover(
    function(){
        $(this).addClass('active');
    },
    function(){
        $(this).removeClass('active');
    }
);

$(".cat-menu ul li").hover(
    function(){
        $(this).addClass('active');
    },
    function(){
        $(this).removeClass('active');
    }
);

var M1  = new allMenu($(".menu").get(0));
var M2  = new allMenu($(".menu").get(1));
M1.init(); M2.init();

//-- PHONE MENU (b) --
var M3  = new phoneMenu($(".phone-menu").get(0));
M3.init();
//  - menu -
M3.el.find('.main_parent').click(function(){
    $(this).toggleClass('active');
});

//  - search(b) -
M3.el.find('.search_parent').click(function(){
    if ( $(this).hasClass('deactive') ){
        $(this).removeClass('active');
        $(this).removeClass('deactive');
    }else{
        $(this).addClass('active');
    }    
});

M3.el.find('.s_close').click(function(){
    $(this).parents('.search_parent').addClass('deactive');
});
//  - search(e) -

//-- PHONE MENU (b) --
$( window ).resize(function() { M1.init(); M2.init(); M3.init();});

/* = = = = = = = = = = = = = = = = = = = = = = = */
/*                  EYEGLASS                     */

var eyeglass = undefined;

$('a.m_image img').mouseenter(function(){
    $(this).parents('div').append('<div class="eyeglass" style="display:none;"></div>');
    eyeglass = $('.eyeglass', $(this).parents('div.egwrap'));
    eyeglass.css({
        background:'black', 
        width:$(this).parents('div.egwrap').outerWidth(), 
        height:$(this).parents('div.egwrap').outerWidth(),
        position:'absolute',
        top:0,
        left:0 + $(this).parents('div.egwrap').outerWidth(),
        'z-index':3,
        'background-image':'url('+$(this).parents('a').attr('href')+')',
        'background-size':'200%',
        'background-repeat':'no-repeat'
    }).fadeIn(200);
}).mouseleave(function(){
    $(eyeglass).fadeOut(200);
});

$('a.m_image img').on( "mousemove", function( event ) {
  //console.log( "pageX: " + event.pageX + ", pageY: " + event.pageY );
  var X = event.pageX - $(this).offset().left;
  var Y = event.pageY - $(this).offset().top;
  //console.log($(this).offset().left);
  eyeglass.css({'background-position-x': -X, 'background-position-y': -Y});
});

/* = = = = = = = = = = = = = = = = = = = = = = = */
/*               HEADER FANCYBOX                 */
$(document).ready(function(){
    $('.top a#feedback, .top a#return_call').fancybox({
            maxWidth	: 800,
            maxHeight	: 600,
            fitToView	: false,
            width		: '70%',
            height		: '70%',
            autoSize	: false,
            closeClick	: false,
            openEffect	: 'none',
            closeEffect	: 'none'
        });
});    