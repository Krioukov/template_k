$(document).ready(function(){
    $('div.product-thumb').hover(function(){
        if($(window).width() > 478){
            if(typeof($(this).parent('div.product-grid').get(0)) !== 'undefined'){
                var ph  = $(this).parent('div.product-grid').height();
            }else{
                var ph  = $(this).parent('div.product-list').height();
            }	
            var w   = $(this).width();
            var o   = $(this).offset();
            var l   = o.left;
            var t   = o.top; 
            var il  = $('img', this).offset().left;
            $(this).addClass('hover').width(w).offset({'top':t-1, 'left':l-1}).css('z-index', '10000');
            if(typeof($(this).parent('div').get(0)) !== 'undefined'){
                $('div.show_on_hower',this).show();	
                $(this).parent('div').height(ph);
            }else{
                $(this).parent('div').height(ph);
            }
            $('img', this).offset({'left':il});
        }
    }).mouseleave(function(){
        if($(window).width() > 478){
            $(this).removeClass('hover').width('auto').css('z-index', 'auto');
            if(typeof($(this).parent('div').get(0)) !== 'undefined'){
                $('div.show_on_hower',this).hide();
            }
            if(typeof($(this).parent('div').get(0)) !== 'undefined'){
                $(this).parent('div').height('auto');
            }else{
                $(this).parent('div').height('auto');
            }	
            $('img', this).css({'position':''});
        }	
    });
});    